
<?php
  require("script_print_file_alim.php");
  ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Modifica alimenti amministratore</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" href="stileCSS.css">
  <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <script type="application/javascript" src="js_MAA.js"> </script>
  <script type="application/javascript" src="controlla_notifiche.js"></script>
</head>
<body>

  <div class="container-fluid">

    <div class="row riga1">
      <div class="col-sm-12">
        <h1 class="rigaTitolo"> Il Girasole </h1>
      </div>
    </div>



    <div class="row  justify-content-center menuNavigazione" id="menuNavigazione">
      <div class="col-sm-4">
        <nav class="navbar navbar-light light-blue lighten-4">
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".menuSelezioneInterno"
        aria-controls="menuSelezioneInterno" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
    <a class="navbar-brand" href="#"></a>
    <div class="collapse navbar-collapse menuSelezioneInterno">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item disabled"><a class="nav-link menuSelezione"> Modifica menù </a></li>
              <li class="nav-item"><a class="nav-link menuSelezione" href="HA.php"> Home </a></li>
              <li class="nav-item"><a class="nav-link menuSelezione" href="pagina_ANAA.php"> Aggiungi al menù </a></li>
              <li class="nav-item"><a class="nav-link menuSelezione" href="pagina_NA.php"> Notifiche </a></li>
              <li class="nav-item"><a class="nav-link menuSelezione" id="logout" href="logout.php"> Logout </a></li>
            </ul>
          </div>
        </nav>
    </div>
  </div>



<div class="row main">
  <div class="col-sm-12">
    <main>
      <div class="row justify-content-center section">
        <div class="col-sm-10">
            <section>
              <h2 class="h2"> Menù </h2>
              <ul class="nav lista_alimenti" id="lista_amministratore"></ul>
            </section>
        </div>
      </div>
    </main>
  </div>
</div>



</div>

</body>
</html>
