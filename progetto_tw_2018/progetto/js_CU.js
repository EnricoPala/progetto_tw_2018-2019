$(function(){

  var array = [];
  var contatore = 0 ;
  var somma = 0 ;

//mi creo un magazzino per passare i prodotti da pagina a pagina

  if (localStorage.getItem("prodotti") == null) {
    prodotti = [];
  } else {
    prodotti = JSON.parse(localStorage.getItem("prodotti"));
  }
  //var mostroSomma = $('<p id="mostrosomma"> L\'importo totale da pagare è: ' + somma + "€" + '</p>');

  for(contatore = 0 ; contatore < prodotti.length; contatore ++){

    var contenitore =  $('<li class="contenitore contenitoreCarrello" id="contenitore' + prodotti[contatore].id +'" ></li>');
    var visualizzaCibo =  $('<li class="elemento" id="cartItem' + prodotti[contatore].id +'" >' + prodotti[contatore].nome  +  '</li>');
    var labelQuantitàCibo = $('<label class="labelQuantitàCibo" for="quantita"><strong>Quantità ordinata </strong></label>');
    var quantitaCibo = $('<input type="number"  class="quantityCart" id="quantita" name="'+prodotti[contatore].id+'"value="' + prodotti[contatore].quantitaOrdinata + '" min="1" max="100"/>');
    var eliminaQuantita = $('<p> <a class="eliminaQuantita" value="'+prodotti[contatore].id +'" href="http://localhost:8080/progetto_tw_2018/progetto/pagina_CU.php"> Rimuovi </a>  </p>');

  //  console.log(prodotti[contatore].quantitaRimasta);
    $("#lista_carrello").append(contenitore);
    $("#contenitore"+ prodotti[contatore].id).append(visualizzaCibo);
    $("#contenitore"+ prodotti[contatore].id).append(labelQuantitàCibo);
    $("#contenitore"+ prodotti[contatore].id).append(quantitaCibo);
    $("#contenitore"+ prodotti[contatore].id).append(eliminaQuantita);

    somma = somma + (prodotti[contatore].prezzo*prodotti[contatore].quantitaOrdinata);
  }

$("#pagare").html('<p id="mostrosomma"> Il totale da pagare è: ' + somma + "€" + '</p>');

$(".eliminaQuantita").on("click",function(){

  for (var i = 0; i < prodotti.length; i++) {
    if(prodotti[i].id == $(this).attr("value")){
      prodotti.splice(i,1);
      localStorage.setItem("prodotti",JSON.stringify(prodotti));
      $(this).remove();
      console.log(prodotti);
    }
  }
});

  $(".quantityCart").change(function(){

    for(var i = 0; i < prodotti.length; i++){
      if(prodotti[i].id == $(this).attr("name")){
        var massimoLocale = parseInt(prodotti[i].quantitaOrdinata)+parseInt(prodotti[i].quantitaRimasta);

        console.log(prodotti[i].quantitaOrdinata);
        prodotti[i].quantitaOrdinata = $(this).val();

          console.log("quantità");
        console.log(prodotti[i].quantitaOrdinata);
        console.log("MASSIMO LOCALE");
        console.log(massimoLocale);

        if (parseInt(prodotti[i].quantitaOrdinata)>parseInt(massimoLocale)) {
          prodotti[i].quantitaOrdinata=massimoLocale;
          $(this).val(prodotti[i].quantitaOrdinata);
        } else {
            if (parseInt(prodotti[i].quantitaOrdinata)<1) {
              prodotti[i].quantitaOrdinata=1;
              $(this).val(prodotti[i].quantitaOrdinata);
            }
          }
        prodotti[i].quantitaRimasta = massimoLocale - $(this).val();
        if(prodotti[i].quantitaRimasta < 0 ){
          prodotti[i].quantitaRimasta = 0 ;

      }
      localStorage.setItem("prodotti",JSON.stringify(prodotti));
    }
  }
  prezzoTotale();
  $("#pagare").html('<p id="mostrosomma"> Il totale da pagare è: ' + somma + "€" + '</p>');
});



if(prodotti.length == 0 ){
  var carrelloVuoto = $('<p id="carrelloVuoto"> Attualmente il carrello è vuoto </p>');
  $("#lista_carrello").html(carrelloVuoto);
  $("#btCarrelloCU").prop("disabled",true);
}


function prezzoTotale(){
    somma = 0 ;
    for (var i = 0; i < prodotti.length; i++){
      somma = somma + (prodotti[i].prezzo*prodotti[i].quantitaOrdinata);
  }
}

$("#btCarrelloCU").click(function(){
 $.ajax({
          url: 'script_CU.php',
          method: 'POST',
          data: {'prodotti': JSON.stringify(prodotti)},
          success: function(response){
              //console.log(response);
          },
          error: function(er) {
            var status = er.status;
            var text = er.statusText;
            var message = status + ': ' + text;
            alert(message);
          }
        });
  localStorage.removeItem("prodotti");
  localStorage.removeItem("counter");
  });
});
