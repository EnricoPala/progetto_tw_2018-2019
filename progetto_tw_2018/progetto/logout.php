<?php
if (session_status() == PHP_SESSION_NONE) { session_start(); }
  session_unset();
  session_destroy();

  header('Location: '. $_SERVER['HOST_NAME'] . '/progetto_tw_2018/progetto/pagina_Welcome.php');
?>
