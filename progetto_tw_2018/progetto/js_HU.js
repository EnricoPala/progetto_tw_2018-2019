$(function(){

  $.getJSON("listaAlimenti.json",function(data,status){

    var arrayCibo = [];
    var prodotti;
    var counter = 0;
    var contatoreWhile = 0 ;


  //array per il carrello.La prima volta che entro con un utente l'array è vuoto (carrello vuoto)
  //se navigo sulle pagine, l'array è pieno e quindi quando accedo a determinate pagine riprendo tutto il contenuto di localStorage
    if (localStorage.getItem("prodotti") == null) {
      prodotti = [];
    } else {
      prodotti = JSON.parse(localStorage.getItem("prodotti"));
    }

  //quando carico per la PRIMA volta la lista ALIMENTI
  //per mantenere le quantità salvate anche se si passa da pagina in pagina
  //Se non c'è COUNTER allora lo creo , altrimenti non faccio nulla
    if(localStorage.getItem("counter") == null){
      localStorage.setItem("counter","uno");
    }

  //ciclo tutto il contenuto di listaAlimenti.json
    for (var contatore = 0 ; contatore < data.length ; contatore ++){

      var varId = parseInt(data[contatore].id);
      var varNome = data[contatore].nome;
      var varDescrizione = data[contatore].descrizione;
      var varPrezzo = data[contatore].prezzo;

    /*creo il codice HTML per home utente*/
      //per ogni aliemento creo un contenitore che contiene nome,descrizione,quantità ecc
      var contenitore           =  $('<div class="contenitore" id="contenitore' + contatore +'" ></div>');
      /*l'id degli elementi sono gli id che ci sono nel database e non del contatore del ciclo for*/
      var nomeElementi          = $('<li class="elemento" id="elemento' + varId +'" >' + varNome + '</li>');
      var descrizione           = $('<p class="descrizione" id="descrizione' + contatore + '" > '+ varDescrizione +' </p>');
      /*attributo name gli do il valore corrente del contatore per captare l'evento del bottone*/
      var btAggiungiAlCArrello  = $('<input type="button" id="btAggiungiAlCarrello'+ contatore +'" class="btn btn-primary btAggiungiAlCarrello" value="Aggiungi al carrello" name="'+ contatore +'"/>');
      var prezzo                =  $('<p class="prezzo" id="prezzo' + contatore +'" > Prezzo: '+ varPrezzo + "€" + '</p>');

  /* array associativo che contiene tutto il contenuto di listaAlimenti.json*/
      arrayCibo.push({
        "id": parseInt(data[contatore].id),
        "nome": data[contatore].nome,
        "quantitaOrdinata" : 0,
        "quantitaRimasta" : data[contatore].quantitaRimasta,
        "descrizione" : data[contatore].descrizione,
        "prezzo" : data[contatore].prezzo
      })



  //per visualizzare il codice HTML
      $("#lista_alimenti").append(contenitore);
      $("#contenitore" + contatore).append(nomeElementi);
      $("#contenitore" + contatore).append(descrizione);
      $("#contenitore" + contatore).append(prezzo);
      $("#contenitore" + contatore).append(btAggiungiAlCArrello);


console.log(arrayCibo[contatore].quantitaRimasta);
      if(arrayCibo[contatore].quantitaRimasta == 0 ){
        $("#btAggiungiAlCarrello" + contatore).prop("disabled",true);
      }


  //evento quando clicco sul bottone Aggiungi
      $(btAggiungiAlCArrello).on("click",function(){

          var tmp = arrayCibo[$(this).attr("name")];
          var flag = 0 ;
          var i = 0 ;
          var indice = $(this).attr("name");

  //se l'array è vuoto, aggiungi tranquillamente tmp lo faccio SOLO UNA VOLTA
          if(prodotti.length == 0 ){
            prodotti.push(tmp);
          }
  //IN QUESTO FOR CI ENTRO SEMPRE
  //se l'lelemento è gia dentro a prodotti allora ne aumento la quantitàOrdinata e ne diminuisco la quantitaRimasta
          for ( i = 0 ; i < prodotti.length ; i ++){

            //l'elemento è giapresente quindi ne aumento solo la quantita
            if(tmp.id == prodotti[i].id) {
              prodotti[i].quantitaOrdinata++;
              prodotti[i].quantitaRimasta--;
              flag = 1 ;
              /*console.log("QUANTITA HU ORDINAATA   " + prodotti[i].quantitaOrdinata);
              console.log("QUANTITA HU RIMASTA   " + prodotti[i].quantitaRimasta);*/
              /*quando la quantità ordinata supera la quantità del database* disabilità aggiungi perchè non posso aggiugnere*/
              if(  prodotti[i].quantitaRimasta == 0){
              //vado a prendere il bottone relativo al contenitore che contiene l'elemento selezionato
                $("#btAggiungiAlCarrello" + indice).prop("disabled",true);
              }
            }
          }
          /*se l'elemento non è presente MA c'è almeno un altro elemento allora inserisco il nuovo elemento
           nel array prodotti*/
          if(flag == 0){
            tmp.quantitaRimasta--;
            tmp.quantitaOrdinata++;
            prodotti.push(tmp);
          }

          localStorage.setItem("prodotti",JSON.stringify(prodotti));
          localStorage.setItem("counter","due");
        });

        if(localStorage.getItem("counter") == "due"){

          while(contatoreWhile < prodotti.length){
            for (counter = 0; counter < data.length; counter++){
              if(prodotti[contatoreWhile].nome == data[counter].nome){
                if(prodotti[contatoreWhile].quantitaRimasta == 0){
                  console.log(prodotti[contatoreWhile].quantitaRimasta);
                  $("#btAggiungiAlCarrello"+counter).prop("disabled",true);
                }
              }
            }
            contatoreWhile++;
          }
          contatoreWhile = 0;
        }
      }
  });
});


/*appena parte la schermata la prima volta, tutti i bottoni rimuovi ( perchè non puoi)
rimuovere una quantità <0) sono disabilitati*/
    /*if(prodotti.length == 0  ){
      $("#btRimuovi" + varId).prop("disabled",true);
    }

*//*
if(localStorage.getItem("counter") == "due"){
  console.log(prodotti);

  var r = 0 ;
  var f = 0 ;
  var t = 0 ;



  while(  prodC < prodotti.length ){


    for ( counter = 0 ; counter < data.length; counter++){



      if(prodotti[prodC].nome == data[counter].nome){

        f = counter+1;

        $('#btAggiungiHU'+f).prop("disabled",true);
      }
    }
      prodC ++;
  }

}
*/

    /*if(localStorage.getItem("counter") == "due"){
      var r = 0 ;
      var f = 0 ;
      var t = 0 ;

      while(t < data.length){
        if(prodotti[t].quantitaOrdinata == 0 ){
          $("#btAggiungiHU" + t).prop("disabled",true);
          $("#contenitore" + t).append('<p class="fraseFineQuantita">Quantità finita nel magazzino!<p>');
        }
      }

}*/

/*
      while(  prodC < prodotti.length ){


        for ( counter = 0 ; counter < data.length; counter++){



          if(prodotti[prodC].nome == data[counter].nome){

            f = counter+1;

            $("#qO" + f).html(" Quantità ordinata:" + prodotti[prodC].quantitaOrdinata);
            $("#qR" + f).html(" Quantità rimasta:" + prodotti[prodC].quantitaRimasta);
            $("#prezzo" + f).html("Prezzo: "+ prodotti[prodC].quantitaOrdinata*prodotti[prodC].prezzo + "€");
            if(prodotti[prodC].quantitaOrdinata == 50){
            $("#btRimuovi" + f).prop("disabled" , true);
          }else{

            $("#btRimuovi" + f).prop("disabled" , false);
          }
          }
        }
          prodC ++;
      }

}*/
