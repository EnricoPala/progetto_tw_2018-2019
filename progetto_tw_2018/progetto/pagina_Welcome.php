
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Pagina di benvenuto</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" href="stileCSS.css">
  <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>
<body>

<div class="container-fluid">

    <div class="row justify-content-center">
      <div class="col-sm-12">
      <h1 id="titoloPrimaPagina"> IL GIRASOLE </h1>
      <h2 id="semprePerPrimaPagina">Pizzeria a domicilio per il campus di Cesena </h2>
    </div>
    </div>

    <div class="row justify-content-md-center" id="soloPerLaPrimaPagina">
      <div class="col-sm-12 primaP">
        <form>
          <div class="btn-group-lg" id="bottoni">
            <input type="button" class="btn btn-primary btn-lg bottoniPrimaPagina" id="bt1" value="Accedi come Admin"
            onclick="window.location.href='pagina_LA.php'"/>
            <input type="button" class="btn btn-primary btn-lg bottoniPrimaPagina" id="bt2" value="Accedi come Utente"
            onclick="window.location.href='pagina_LU.php'"/>
          </div>
        </form>
      </div>
    </div>


</div>
</body>
</html>
