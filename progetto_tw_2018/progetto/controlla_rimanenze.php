<?php

  if (session_status() == PHP_SESSION_NONE) { session_start(); }


  if ($_SESSION['id']==1) { // if admin is logged

    echo "true";

    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "progetto_db";

  // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    $quantitaRimastaMin = 15;

    $stmt = $conn->prepare("SELECT id, nome, quantitaRimasta FROM lista_alimenti WHERE (quantitaRimasta<=?)");

    $stmt->bind_param("i", $quantitaRimastaMin);
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($id, $nome, $quantitaRimasta);

    if ($stmt->num_rows>0) {
      while ($stmt->fetch()) {

        date_default_timezone_set("Europe/Rome");

        $id_user = $_SESSION['id'];
        $messaggio = " Quantità di  " . $nome . " rimasta è " . $quantitaRimasta . ". Contatta il tuo fornitore.";
        $letto = 0;
        $data = date("Y-m-d") . " " . date("G:i:s");

        $stmt_check_message = $conn->prepare("SELECT messaggio FROM notifiche WHERE utente_id=?");
        $stmt_check_message->bind_param("i", $id_user);
        $stmt_check_message->execute();
        $stmt_check_message->store_result();
        $stmt_check_message->bind_result($m);

        $condition = false;

        if ($stmt_check_message->num_rows > 0) {
          while ($stmt_check_message->fetch()) {
            if ($messaggio == $m) {
              $condition = true;
              echo "false";
              break;
            }
          }
        }

        $stmt_check_message->free_result();

        if (!$condition) {
          $stmtNotifiche = $conn->prepare("INSERT INTO notifiche (utente_id, messaggio, letto, data) VALUES (?, ?, ?, ?)");
          $stmtNotifiche->bind_param("isis", $id_user, $messaggio, $letto, $data);
          $stmtNotifiche->execute();
          $stmtNotifiche->close();
        } else {
          echo "false";
        }
      }
    } else {
      echo "false";
    }

    $stmt->close();

    $conn->close();

  } else {
    echo "false";
  }

?>
