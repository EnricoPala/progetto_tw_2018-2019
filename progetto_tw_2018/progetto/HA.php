
<?php
  require("script_print_file_alim.php");
  ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Home Amministratore</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1" >
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" href="stileCSS.css">
  <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <script type="text/javascript" src="controlla_notifiche.js"></script></head>
<body>

  <div class="container-fluid">

    <div class="row riga1">
      <div class="col-sm-12">
        <h1 class="rigaTitolo"> Il Girasole </h1>
      </div>
    </div>

    <div class="row justify-content-center menuNavigazioneAdmin" id="menuNavigazioneAmministratore">
      <div class="col-sm-10">
        <nav class="navbar navbar-light">
          <div class="container-fluid">
              <input type="button" class="btn btn-primary btn-lg btn-block bottoniAdmin" id="btA1" value="Modifica menù" onclick="window.location.href='pagina_MAA.php'"/>
              <input type="button" class="btn btn-primary btn-lg btn-block bottoniAdmin" id="btA2"value="Notifiche" onclick="window.location.href='pagina_NA.php'"/>
              <input type="button" class="btn btn-primary btn-lg btn-block bottoniAdmin" id="btA3"value="Aggiungi al menù" onclick="window.location.href='pagina_ANAA.php'"/>
              <input type="button" class="btn btn-primary btn-lg btn-block bottoniAdmin" id="btA4"value="Logout" onclick="window.location.href='logout.php'"/>
          </div>
        </nav>
    </div>
  </div>


</div>

</body>
</html>
