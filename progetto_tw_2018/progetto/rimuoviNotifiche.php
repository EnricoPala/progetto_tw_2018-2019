<?php

if (session_status() == PHP_SESSION_NONE) { session_start(); }

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "progetto_db";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


$id_current_user = $_SESSION['id'];

$stmt_delete = $conn->prepare("DELETE FROM notifiche WHERE utente_id=?");

$stmt_delete->bind_param("i", $id_current_user);
$stmt_delete->execute();

$stmt_delete->close();
$conn->close();

if ($id_current_user == 1) { // if admin
  header('Location: '. $_SERVER['HOST_NAME'] . '/progetto_tw_2018/progetto/pagina_NA.php');
} else {
  header('Location: '. $_SERVER['HOST_NAME'] . '/progetto_tw_2018/progetto/pagina_MNU.php');
}

?>
