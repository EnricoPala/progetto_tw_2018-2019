<?php

  if (session_status() == PHP_SESSION_NONE) { session_start(); }

  $servername = "localhost";
  $username = "root";
  $password = "";
  $dbname = "progetto_db";

// Create connection
  $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  }

  $idCorrente = $_SESSION['id'];
  $nonLetto = 0;

  $stmt = $conn->prepare("SELECT utente_id, messaggio, letto FROM notifiche WHERE utente_id=? AND letto=?");

  $stmt->bind_param("ii", $idCorrente, $nonLetto);
  $stmt->execute();
  $stmt->store_result();
  $stmt->bind_result($id, $messaggio, $letto);
  $stmt->fetch();

  if ($stmt->num_rows>0) {
    echo "Nuova notifica!!! Ordine effettuato!";
  } else {
    echo "false";
  }

  $conn->close();

?>
