<?php

ob_start();
if (session_status() == PHP_SESSION_NONE) { session_start(); }


if(isset($_POST['prodotti'])){
/*  echo "CI ENTRO";
*/
$_SESSION['carrello'] = $_POST['prodotti'];



$servername = "localhost";
$username = "root";
$password = "";
$dbname = "progetto_db";


// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


  $carrello = json_decode($_POST['prodotti'],true);


if (count($carrello) > 0){


  $stmtListaAlimenti = $conn->prepare("SELECT quantitaRimasta FROM lista_alimenti WHERE (id=?)");
  $stmtUpdate = $conn->prepare("UPDATE lista_alimenti SET quantitaRimasta=? WHERE id=?");
 $stmtNotifiche = $conn->prepare("INSERT INTO notifiche (utente_id, messaggio, letto, data) VALUES (?, ?, ?, ?)");

//  echo "STAMPO L'ARRAY LA CHIAVE èL'INDICE DELL'ARRAY , IL VALORE INVECE SONO OGNI ALIMENTO QUINDI ID ECC ECC";
  foreach ($carrello as $key => $value) {
    $quantitaOrdinata=0;
    $result;
  /*  print_r($key);
    print_r($value);*/
   foreach ($value as $k => $v) {
    /* echo "chiave";
     print_r($k);
     echo "valore";
     print_r($v);*/

     if($k == "id"){
       $valoreId = $v;
       $stmtListaAlimenti->bind_param("i",$valoreId);
       $stmtListaAlimenti->execute();
       $stmtListaAlimenti->bind_result($result);
       $stmtListaAlimenti->fetch();
       $stmtListaAlimenti->store_result();
       $magazzino = $result;
    /* echo "TROVATOOOOOOOOOOOOOOOOOOOOOOOOOOO";
       print_r($magazzino);*/


     }
       if ($k == 'quantitaOrdinata') {
                $quantitaOrdinata = $v;
              }
}

  //  echo " PERCHE NON LO VEEDO? $quantitaOrdinata";
        $nuoveQuantita = ($magazzino - $quantitaOrdinata);
    //    echo"NUOVA QUANTITA $nuoveQuantita";
        $stmtListaAlimenti->free_result();
        $stmtUpdate->bind_param("ii", $nuoveQuantita, $valoreId);
        $stmtUpdate->execute();
        $stmtUpdate->store_result();
        $stmtUpdate->free_result();
      }

      date_default_timezone_set("Europe/Rome");

    $id_user = $_SESSION['id'];
    $message = "Ordine partito.";
    $letto = 0;
    $data = date("Y-m-d") . " " . date("G:i:s");

    $stmtNotifiche->bind_param("isis", $id_user, $message, $letto, $data);
    $stmtNotifiche->execute();
    $stmtNotifiche->store_result();
    $stmtNotifiche->free_result();

    $stmtListaAlimenti->close();
    $stmtUpdate->close();
    $stmtNotifiche->close();

    $_SESSION['ordine']=true;


print_r($_SESSION);
}
$conn->close();

}

header('Location: '. $_SERVER['HOST_NAME'] . '/progetto_tw_2018/progetto/HU.php');

ob_end_flush();

?>
