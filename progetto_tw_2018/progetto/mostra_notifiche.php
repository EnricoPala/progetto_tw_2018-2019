<?php

if (session_status() == PHP_SESSION_NONE) { session_start(); }

          $servername = "localhost";
          $username = "root";
          $password = "";
          $dbname = "progetto_db";

        // Create connection
          $conn = new mysqli($servername, $username, $password, $dbname);
          // Check connection
          if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
          }

$result;

$id_current_user = $_SESSION['id'];


$stmt_select = $conn->prepare("SELECT messaggio, letto, data FROM notifiche WHERE utente_id=? ORDER BY data DESC");

$stmt_select->bind_param("i", $id_current_user);
$stmt_select->execute();
$stmt_select->store_result();
$stmt_select->bind_result($messaggio, $letto, $data);


if ($stmt_select->num_rows>0) {

    while ($stmt_select->fetch()) {

      if ($letto == 1) {
        echo '<li class="notificaLetta"><h4>Notifica letta</h4><p class="messaggio-notifica">' . $data . ' : '. $messaggio . '</p></li>';
      } else {
        echo '<li class="notificaNuova"><h4>NUOVA NOTIFICA!!!</h4><p class="messaggio-notifica">' . $data . ' : ' . $messaggio . '</p></li>';
      }

      // <h4 class=\"bg-dark text-light\">Messaggio Letto:</h4><p class=\"lettura-notifica\">" . $letto . "</p>

    }
    $stmt_select->free_result();

    $stmt_update = $conn->prepare("UPDATE notifiche SET letto='1' WHERE utente_id=?");
    $stmt_update->bind_param("i", $id_current_user);
    $stmt_update->execute();


    $stmt_update->close();
} else {
  echo "false"; // no notification at all
}

$stmt_select->close();
$conn->close();

?>
