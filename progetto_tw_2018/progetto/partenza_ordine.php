<?php

    if (session_status() == PHP_SESSION_NONE) { session_start(); }

    // print_r($_SESSION);

    if (isset($_SESSION['ordine'])) { // se la variabile è stata inizializzata
      if ($_SESSION['ordine'] == true && $_SESSION['id']!=1) { // se la variabile è uguale a true e se non è entrato l'amministratore

        echo "true";


          $servername = "localhost";
          $username = "root";
          $password = "";
          $dbname = "progetto_db";

        // Create connection
          $conn = new mysqli($servername, $username, $password, $dbname);
          // Check connection
          if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
          }


          $stmt_insert_notification = $conn->prepare("INSERT INTO notifiche (utente_id, messaggio, letto, data) VALUES (?, ?, ?, ?)");


          date_default_timezone_set("Europe/Rome");

          $id_user = $_SESSION['id'];
          $message = "Ordine consegnato!";
          $letto = 0;
          $data = date("Y-m-d") . " " . date("G:i:s");

          $stmt_insert_notification->bind_param("isis", $id_user, $message, $letto, $data);
          $stmt_insert_notification->execute();

          $stmt_insert_notification->close();

          $conn->close();

          $_SESSION['ordine'] = false;

      } else {
        echo "false";
      }
    } else {
      echo "false";
    }
