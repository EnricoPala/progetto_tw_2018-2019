<!DOCTYPE html>
<html lang="it">
<head>
  <title>Login Utente Completo</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1" >
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" href="stileCSS.css">
  <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</head>
  <body>
    <div class="container-fluid">

      <div class="row riga1">
        <div class="col-sm-12">
          <h1 class="rigaTitolo"> Il Girasole </h1>
        </div>
      </div>



      <div class="row justify-content-center loginCompleto">
        <div class="col-sm-11">
          <div class="form-group">
            <form method="post" action="script_LUC.php" class="form">
              <fieldset>
                <legend class="legendLU"> Inserisci i dati richiesti </legend>
                  <div class="row  justify-content-center">
                    <div class="col-sm-10">
                    <label for="inputNome" class="scrittaForm"> </label>
                    <input type="text" id="inputNome" name="nome"  class="form-control campi campiA" placeholder="Nome" required> </input>

                    <label for="inputCognome" class="scrittaForm"> </label>
                    <input type="text" id="inputCognome" name="cognome"   class="form-control campi campiA" placeholder="Cognome" required> </input>

                    <label for="inputAnno" class="scrittaForm"> </label>
                    <input type="text" id="inputAnno" name="annoDiNascita" class="form-control campi campiA" placeholder="Data di nascita" onfocus="(this.type='date')" onblur="(this.type='text')" max="2000-01-01" min="1900-01-01" required> </input>

                    <label for="inputEmail" class="scrittaForm"> </label>
                    <input type="text" id="inputEmail" name="indirizzoEmail"  class="form-control campi campiA" placeholder="Indirizzo email" required> </input>

                    <label for="inputPassword" class="scrittaForm"> </label>
                    <input type="password" id="inputPassword" name="password" class="form-control campi campiA" placeholder="Password" required> </input>

                    <input type="submit"  class="btn btn-primary btn-lg bottoni_LUC bottoni_LUCA" value="Login"/>
                    <input type="button"  class="btn btn-primary btn-lg bottoni_LUC bottoni_LUCA bottoneLUC" value="Indietro"   onclick="window.location.href='pagina_LU.php'" />
                  </div>
                </div>
              </fieldset>
            </form>
      </div>
    </div>
  </div>
</div>
</body>
</html>
