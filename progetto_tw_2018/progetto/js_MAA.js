$(function(){

  $.getJSON("listaAlimenti.json", function(data, status){

    var arrayCibo = [];

    for (var contatore = 0; contatore < data.length; contatore++) {


      var varId       = parseInt(data[contatore].id);
      var varNome     = data[contatore].nome;
      var varQRimasta = data[contatore].quantitaRimasta;

      console.log(data[contatore]);
      console.log(varNome);

      var formModificaAlimenti     =  $('<form method="post" action="script_MAA.php" class="formModificaAlimenti" id="formModificaAlimenti'+contatore+'">');
      var nomeElementi             =  $('<li class="liMAA" id="elementoA' + varId +'" >' + varNome + '</li>');
      var elementoId               =  $('<label class="labelID" for="id">ID: </label><input type="text" class="ultimaClasse" id="id" name="id" value="' + varId + '" class="listino-item-id" readonly></input>');
      var quantitaRimasta          =  $('<p class="quantitaRimasta" id="qRA' + varId +'" > Quantità rimasta nel magazzino: '+ varQRimasta + '</p>');
    //  var divPerCSS                =  $('<div class="divPerCSS">');
      var btIncrementaMax          =  $('<input type="submit" id="btIncrementa'+ varId +'" class="btn btn-primary incrementaMax" value="Ripristina" name="btIncrementaMax"/>');
      var btRimuoviDalMagazzino    =  $('<input type="submit" class="btn btn-primary  incrementaMax"  value="Rimuovi" name="btRimuoviDalMagazzino" />');

      arrayCibo.push({
        "id" : parseInt(data[contatore].id), // id from sql is saved as string. parseInt converts it into a number
        "nome" : data[contatore].nome,
        "quantitaRimasta" : data[contatore].quantitaRimasta
      });

      $("#lista_amministratore").append(formModificaAlimenti);
      $("#formModificaAlimenti" + contatore).append(nomeElementi);
      $("#formModificaAlimenti" + contatore).append(elementoId);
      $("#formModificaAlimenti" + contatore).append(quantitaRimasta);
  //    $("#formModificaAlimenti" + contatore).append(divPerCSS);
      $("#formModificaAlimenti" + contatore).append(btRimuoviDalMagazzino);
      $("#formModificaAlimenti" + contatore).append(btIncrementaMax);

      if(arrayCibo[contatore].quantitaRimasta == 50){
        $("#btIncrementa"+ varId).prop("disabled",true);
      }

     $(btRimuoviDalMagazzino).on("click",function(){

//e.preventDefault();
        var messaggio = $("<p> Alimento rimosso dal magazzino, perfavore ricarica la pagina web </p>")
        $("#elementoA" + varId).append(messaggio);
        var tmp = arrayCibo[$(this).attr("name") - 1];
        var n = $(this).attr("name");
        console.log(tmp);
        $.ajax({
                   url: 'script_MAA.php',
                   method: 'POST',
                   data: {'alimento': JSON.stringify(tmp.id)},
                   success: function(response){
                     //alert(response);
                     console.log(response);
                   },
                   error: function(er) {
                     console.log(er.status);
                       var status = er.status;
                       var text = er.statusText;
                       var message = status + ': ' + text;
                       alert(message);
                   }
               });
      });

    }
  });
});
